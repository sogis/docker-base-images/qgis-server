# qgis-server

**Achtung:** ARM-Images verwenden QGIS-Server 3.10 (aus dem offiziellen Repo).

MacOS: `docker run -p 8083:80 -e QGIS_FCGI_MIN_PROCESSES=2 -e QGIS_FCGI_MAX_PROCESSES=2 sogis/oereb-wms:2.0` FCGI_MIN- und Max-Prozesse setzen. Ansonsten geht nach circa 10 Minuten nichts mehr mit QGIS-Server. Im error.log steht dann "can't apply process slot for /usr/lib/cgi-bin/qgis_mapserv.fcgi". -
Nein, betrifft auch Ubuntu.